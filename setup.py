#!/usr/bin/env python

from distutils.core import setup

import hunpy

setup(name='Hunpy',
    version='0.2',
    description='Python wrapper for the hunspell library',
    author='David Mugnai',
    author_email='dvd@gnx.it',
    url='',
    packages=['hunpy'],
    ext_package='hunpy',
    ext_modules=[hunpy.ffi.verifier.get_extension()]
)
