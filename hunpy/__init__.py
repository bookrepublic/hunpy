# -*- coding: UTF-8 -*-
from cffi import FFI

ffi = FFI()
ffi.cdef('''
typedef struct Hunhandle Hunhandle;

Hunhandle *Hunspell_create(const char * affpath, const char * dpath);
void Hunspell_destroy(Hunhandle *pHunspell);
int Hunspell_stem(Hunhandle *pHunspell, char***slst, const char *word);
int Hunspell_analyze(Hunhandle *pHunspell, char*** slst, const char * word);
char *Hunspell_get_dic_encoding(Hunhandle *pHunspell);

int Hunspell_stem2(Hunhandle *pHunspell, char*** slst, char** desc, int n);
''')

HUNLIB = ffi.verify('''
#include <hunspell/hunspell.h>
''', libraries=['hunspell-1.3'], ext_package='hunpy')

DICT_SEARCH_PATH = ()

def search_dict(language):
    import os, os.path
    for base in DICT_SEARCH_PATH:
        for f in os.listdir(base):
            if os.path.splitext(f)[0] != language:
                continue
            aff = os.path.join(base, language) + '.aff'
            dic = os.path.join(base, language) + '.dic'
            print aff, base
            if os.path.isfile(aff) and os.path.isfile(dic):
                return aff, dic
    return None, None

class Stemmer(object):
    def __init__(self, language=None, aff=None, dic=None):
        if aff and dic:
            pass
        elif language:
            aff, dic = search_dict(language)
        else:
            aff = dic = None

        if not all((aff, dic)):
            raise ValueError("Dictionary not specified")

        self.handle = HUNLIB.Hunspell_create(aff, dic)
        self.dict_encoding = ffi.string(HUNLIB.Hunspell_get_dic_encoding(self.handle))

    def close(self):
        HUNLIB.Hunspell_destroy(self.handle)
        self.handle = None

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()
        return False

    def word(self, word):
        output = []

        enc = self.dict_encoding
        if isinstance(word, unicode):
            word = word.encode(enc)

        buff = ffi.new('char***')
        count = HUNLIB.Hunspell_stem(self.handle, buff, word)
        for ix in range(count):
            output.append(unicode(ffi.string(buff[0][ix]), enc))

        return output

    __call__ = word
